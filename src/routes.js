import React from 'react'
import { Route, Switch, Redirect } from 'react-router'
import { routes } from './helpers'


export default (props) => {
  const renderMergedProps = (component, ...rest) => {
    const finalProps = Object.assign({}, ...rest)
    return React.createElement(component, finalProps)
  }

  const PropsRoute = ({ component, ...rest }) => {
    return !rest.redirect ? (
      <Route {...rest} render={routeProps => renderMergedProps(component, routeProps, rest)} />
    ) : <Redirect to={rest.redirect} />
  }

  return (
    <Switch>
      {routes.map(route => (
        <PropsRoute key={route.id} {...route} {...props} />
      ))}
    </Switch>
  )
}
