import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom'
import { Main } from './components/templates';
import Routes from './routes';
import { Login } from './components/pages';
import { isAuthenticated } from './helpers';

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";


const PrivateRoute = ({ children, ...rest }) => {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        isAuthenticated() ? (
          children
        ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: location }
              }}
            />
          )
      }
    />
  );
}

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" render={() => (
          !isAuthenticated() ? <Redirect to="/login" /> : <Redirect to="/dashboard" />
        )} />
        <Route path='/login'>
          {!isAuthenticated() ? <Login /> : <Redirect to="/dashboard" />}
        </Route>
        <PrivateRoute path='/'>
          <Main>
            <Routes />
          </Main>
        </PrivateRoute>
      </Switch>
    </Router>
  )
}

export default App;

