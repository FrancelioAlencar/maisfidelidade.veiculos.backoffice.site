import { FetchLogin } from "../services";
import { setCookie } from "../helpers";

const Login = ({ login, password, check }) => {
  return new Promise((resolve, reject) => {
    console.log({ check })
    FetchLogin({ login, password }).then(response => {
      var jwtPayload = response.replace(/_/g, "/").replace(/-/g, "+");
      var result = JSON.parse(
        decodeURIComponent(escape(atob(jwtPayload.split(".")[1])))
      );
      if (result.Perfil == 1 || result.Perfil == 3) {
        setCookie("BackOffice.Login", response, 600000, check);
        resolve(true);
      } else {
        alert("Você não tem permissão para fazer o login");
        reject(false);
      }
    });
  });
};

export default Login;
