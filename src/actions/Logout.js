import { removeCookie } from "../helpers";


const Logout = () => {
    removeCookie("BackOffice.Login");
    window.location.pathname = '/login'
};

export default Logout;
