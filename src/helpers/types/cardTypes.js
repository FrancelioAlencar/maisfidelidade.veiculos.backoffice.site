import { IconPlus } from "../../assets/index";

export const upload = {
  color: "#1cb2bd",
  width: 260,
  height: 120,
  image: true,
  icon: IconPlus,
  colorText: "#ffffff",
  text: "Fazer upload de um novo arquivo"
};
export const register = {
  color: "#ffffff",
  width: 260,
  height: 120,
  colorInfo: "#1cb2bd",
  textInfo: "35000",
  colorText: "#34303f",
  text: "Registros processados no mês."
};
export const errors = {
  color: "#ffffff",
  width: 260,
  height: 120,
  colorInfo: "#1cb2bd",
  textInfo: "33",
  colorText: "#34303f",
  text: "Erros encontrados nos registros processados."
};
export const files = {
  color: "#ffffff",
  width: 260,
  height: 120,
  colorInfo: "#1cb2bd",
  textInfo: "03",
  colorText: "#34303f",
  text: "Arquivos processados no mês."
};
