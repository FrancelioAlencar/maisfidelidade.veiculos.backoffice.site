const sizeVw = size => {
    return window.innerWidth <= 1440 ? `${(size * 133.34) / 1920}vw` : `${size * 1.2}px`
};
export default sizeVw;
