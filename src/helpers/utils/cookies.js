export const setCookie = (cname, cvalue, minutes, expire) => {
  var d = new Date();
  d.setTime(d.getTime() + (minutes * 60 * 1000));
  var expires = "expires=" + d.toUTCString() + ";";
  document.cookie = `${cname}=${cvalue}; ${expire ? expires : ''}`;
}

export const isAuthenticated = () => {
  let cookie = document.cookie
  return cookie && cookie.indexOf('BackOffice.Login') !== -1
}

export const getCookie = cname => {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return null;
}

export const getCookieData = () => {
  let cookie = getCookie('BackOffice.Login')
  if (cookie) {
    var jwtPayload = cookie.replace(/_/g, "/").replace(/-/g, "+");
    var result = JSON.parse(
      decodeURIComponent(escape(atob(jwtPayload.split(".")[1])))
    );
    return result
  }
  return null
}

export const removeCookie = (name) => {
  if (getCookie(name)) {
    document.cookie = name + "=" +
      ";expires=Thu, 01 Jan 1970 00:00:01 GMT";
  }
}