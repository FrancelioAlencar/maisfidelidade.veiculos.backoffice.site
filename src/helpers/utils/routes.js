import { Dashboard, Extract, Upload } from "../../components/pages/";

import { IconDashboard, IconUpload, IconReport } from "../../assets";
import Rescue from "../../components/pages/Rescue";

const routes = [
  {
    id: 0,
    path: "/dashboard",
    exact: true,
    src: IconDashboard,
    component: Dashboard,
    subLinks: [
      {
        title: "Dashboard"
      }
    ]
  },
  {
    id: 1,
    path: "/upload",
    src: IconUpload,
    component: Upload,
    subLinks: [
      {
        title: "Upload"
      }
    ]
  },
  {
    id: 2,
    path: "/relatorio",
    exact: true,
    src: IconReport,
    redirect: "/relatorio/extrato",
    subLinks: [
      {
        title: "Relatório",
        lst: [
          { id: 0, name: "Extratos", path: "/relatorio/extrato" },
          { id: 1, name: "Resgates", path: "/relatorio/resgate" }
        ]
      }
    ]
  },
  {
    id: 3,
    path: "/relatorio/extrato",
    component: Extract
  },
  {
    id: 4,
    path: "/relatorio/resgate",
    component: Rescue
  }
];

export default routes;
