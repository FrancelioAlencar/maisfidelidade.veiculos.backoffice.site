import React from "react";
import { BubbleContainer, Label, Image, Container } from "../../atoms";

import {
    Table,
    Row,
    Pipe,
    Icon,
    TextRow,
} from "./styled";

import { IconDownload } from "../../../assets";

let mockMonth = [
    { id: 0, name: 'Mes04.xls', date: '01/ABR' }
]

const ReportRescue = () => (
    <Container marginBottom={42}>
        <BubbleContainer title="Relatório de resgate" />
        <Table>
            {
                mockMonth.map(item => (
                    <Row>
                        <TextRow>{item.name}</TextRow>
                        <Label cursor="pointer" size={15} color="#eb0c2f" marginRight={20}>ATUALIZAR AGORA</Label>
                        <Pipe />
                        <Icon>
                            <Image src={IconDownload} width={20} />
                        </Icon>
                    </Row>
                ))
            }
        </Table>
    </Container>
);

export default ReportRescue;