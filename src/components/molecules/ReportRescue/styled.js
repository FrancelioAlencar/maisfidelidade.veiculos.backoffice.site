import styled from "styled-components";
import { sizePxToVw } from "../../../helpers";

export const Container = styled.section`
  width: calc(100% - ${sizePxToVw(80)});
  min-height: 80px;

  margin-bottom: 20px;

  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  background-color: #fff;
  border-radius: 10px;
  padding: 0px ${sizePxToVw(40)};
  position: relative;

  &:after {
    content: "";
    width: 0;
    height: 0;
    border-top: 20px solid transparent;
    border-left: 15px solid #fff;
    border-bottom: 20px solid transparent;
    position: absolute;
    left: ${sizePxToVw(90)};
    top: calc(100% - 15px);
    transform: rotate(90deg);
    margin-bottom: 10px;
    margin-left: 60px;
  }
`;
export const Row = styled.div`
  display: flex;
  flex-direction: row;
  border-collapse: collapse;
  align-items: center;

  height: ${sizePxToVw(60)};
  background-color: #f0f1f5;

  padding: 0px ${sizePxToVw(40)};
`;

export const TextRow = styled.span`
  display: flex;
  flex: 1;
  font-family: "Open Sans", sans-serif;
  font-size: ${sizePxToVw(18)};
  text-align: left;
  color: #656565;
  font-weight: bold;
`;

export const DateRow = styled.span`
  width: 46px;
  height: 20px;
  font-family: "Open Sans";
  font-size: 15px;
  line-height: 1.33;
  letter-spacing: -0.38px;
  color: #eb0c2f;
  margin-right: 20px;
`;

export const Pipe = styled.div`
  height: 50%;
  width: 0px;
  background-color: #707070;
  opacity: 0.2;
  border: solid 1px #707070;
`;
export const Icon = styled.div`
  display: flex;
  justify-content: center;
  width: 5%;
`;

export const Table = styled.div`
  width: 100%;
  border: solid 3px #dbdbdb;
  border-radius: 1em;

   ${Row}:nth-child(2n + 1) {
    background-color: #fff;
  }
   ${Row}:first-child {
    border-top-left-radius: 1em;
    border-top-right-radius: 1em;
  }
   ${Row}:last-child {
    border-bottom-left-radius: 1em;
    border-bottom-right-radius: 1em;
  }
`;

export const ButtonRescue = styled.div`
  width: ${sizePxToVw(162)};
  height: ${sizePxToVw(30)};
  border-radius: ${sizePxToVw(30)};
  background-color: ${({ rescue }) => rescue ? 'red' : '#e5e5e5'};
  display: flex;
  justify-content: center;
  align-items: center;
  margin-left: 20px;

  &:hover{
    background-color: ${({ rescue }) => rescue ? '#a00000' : ''};
  }

`
