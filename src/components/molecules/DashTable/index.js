import React, { useState, Fragment } from "react";

import { Image, Button, Label, BubbleContainer, Container } from "../../atoms";

import {
  Table,
  Row,
  Pipe,
  Icon,
  TextRow,
  DateRow,
  ButtonRescue
} from "./styles";
import { IconEye, IconDownload } from "../../../assets";

let mock = [
  { id: 0, name: 'Semana45.xls', date: '01/FEV' },
  { id: 1, name: 'Semana46.xls', date: '05/FEV' },
  { id: 2, name: 'Semana47.xls', date: '12/FEV' },
  { id: 3, name: 'Semana48.xls', date: '17/FEV' },
]

const DashTable = ({ data, title }) => {
  const [extract, setExtract] = useState(data || mock)
  return (
    <Container marginBottom={42}>
      <BubbleContainer title={title} />
      <Table>
        {
          extract.map(item => (
            <Row>
              <TextRow>{item.name}</TextRow>
              {
                item.comp ?
                  () => item.comp :
                  <DateRow>{item.date}</DateRow>
              }
              <Pipe />
              <Icon>
                <Image src={IconDownload} width={20} />
              </Icon>
              {item.rescue || item.rescue === false ? (
                <Fragment>
                  <Pipe />
                  <ButtonRescue rescue={item.rescue}>
                    <Label size={12} color="#FFF" weight="600">Liberar resgate</Label>
                  </ButtonRescue>
                </Fragment>
              ) : null}

            </Row>
          ))
        }
      </Table>
    </Container>
  );
};

export default DashTable;
