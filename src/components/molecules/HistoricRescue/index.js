import React from "react";
import { BubbleContainer, Label, Image, Container } from "../../atoms";

import {
    Table,
    Row,
    Pipe,
    Icon,
    TextRow,
    ContainerLabel
} from "./styled";

import { IconDownload } from "../../../assets";

let mockMonth = [
    { id: 0, name: 'Resgate | DEZ | 19', date: '01/FEV' },
    { id: 1, name: 'Resgate | DEZ | 19', date: '05/FEV' },
    { id: 2, name: 'Resgate | DEZ | 19', date: '12/FEV' },
    { id: 3, name: 'Resgate | DEZ | 19', date: '17/FEV' },
]

const HistoricRescue = () => (
    <Container marginBottom={42}>
        <BubbleContainer title="Históricos de Resgates mensais" />
        <Table>
            {
                mockMonth.map(item => (
                    <Row>
                        <TextRow>{item.name}</TextRow>
                        <ContainerLabel>
                            <Label wrap="nowrap" weight="600" size={12} color="#eb0c2f">RESGATE LIBERADO</Label>
                            <Label size={12} color="transparent">{item.date}</Label>
                        </ContainerLabel>
                        <Pipe />
                        <Icon>
                            <Image src={IconDownload} width={20} />
                        </Icon>
                    </Row>
                ))
            }
        </Table>
    </Container>
);

export default HistoricRescue;