import styled from "styled-components";
import { sizePxToVw } from "../../../helpers";

export const HeaderContainer = styled.section`
  width: calc(100% - ${sizePxToVw(39.3 * 2)});
  height: 60px;
  background-color: #a00000;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  padding: 0 ${sizePxToVw(39.3)};
  z-index: 3;
  position: relative;
`;

export const Content = styled.div`
    width: auto;
    height: 100%;
    border-left: 1px solid rgba(255, 255, 255, .2);
    padding: 0 ${sizePxToVw(21.4)};
    display: flex;
    align-items: center;
    cursor: pointer;
`

export const Triangle = styled.span`
  width: 0; 
  height: 0; 
  border-left: 5px solid transparent;
  border-right: 5px solid transparent;
  margin-left: ${sizePxToVw(20)};
  
  border-bottom: 5px solid white;

  transform: rotate(180deg);
  position: relative;
  
`

export const Collapse = styled.div`
    position: absolute;
    bottom: 10px;
    left: -20px;

    display: ${({ open }) => open ? 'flex' : 'none'};
    flex-direction: column;
    width: auto;
    align-items: center;
    background-color: #FFF;
    border-radius: ${sizePxToVw(5)};
    padding: ${sizePxToVw(10)};
    transform: rotate(180deg);
`