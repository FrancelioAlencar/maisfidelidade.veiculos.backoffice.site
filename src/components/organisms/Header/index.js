import React, { useState, useEffect } from 'react'
import { HeaderContainer, Content, Triangle, Collapse } from './styled'
import { getCookieData } from '../../../helpers'
import { Image, Label, Container } from '../../atoms'
import { IconNotification, IconConfig, IconUser } from '../../../assets'
import { DoLogout } from '../../../actions'

const Header = () => {
    const [name, setName] = useState(null)
    const [open, setOpen] = useState(false)
    const [ref, setRef] = useState(null)

    function handleClickOutside(event) {
        if (ref && !ref.contains(event.target)) {
            if (open) setOpen(false)
        }
    }

    useEffect(() => {
        document.addEventListener('mousedown', handleClickOutside)
        return () => {
            document.removeEventListener('mousedown', handleClickOutside)
        }
    })

    useEffect(() => {
        const cookie = getCookieData();
        setName(cookie.Nome)
    }, [])

    return (
        <HeaderContainer>
            <Content>
                <Image src={IconNotification} />
            </Content>
            <Content>
                <Image src={IconConfig} />
            </Content>
            <Content onClick={() => !open ? setOpen(!open) : null} ref={setRef}>
                <Image src={IconUser} border="1px solid white" borderRadius={2} />
                <Container marginLeft={19}>
                    <Label wrap="nowrap" textTransform="capitalize" cursor="pointer" color="#FFF" fontFamily="Helvetica" weight="bold" size={15}>{name ? name.toLowerCase() : ''}</Label>
                    <Label textTransform="capitalize" cursor="pointer" color="#FFF" fontFamily="Helvetica" weight="300" size={12}>Admin</Label>
                </Container>
                <Triangle >
                    <Collapse open={open}>
                        <Label onClick={() => DoLogout()} cursor="pointer" color="#000" fontFamily="Helvetica" weight="bold" size={15}>Sair</Label>
                    </Collapse>
                </Triangle>
            </Content>

        </HeaderContainer>
    )
}

export default Header