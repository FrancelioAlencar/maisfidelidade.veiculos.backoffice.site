import React, { useEffect, useState, Fragment } from "react";
import { withRouter } from "react-router";
import { Card, ItemSliderHeader } from "../../atoms";
import {
  upload,
  register,
  errors,
  files
} from "../../../helpers/types/cardTypes";
import Slider from "react-slick";

import { SubHeaderContainer, SubHeaderContent, CardContainer, DivSlider, Arrow } from "./styles";
import { IconClock } from "../../../assets/";
import { Label, Image, Container } from "../../atoms";
import { getCookieData } from "../../../helpers";

const SubHeader = ({ ...props }) => {
  const [date, setDate] = useState(new Date());
  const [momenteDate, setMomentDate] = useState("");
  const [name, setName] = useState(null)

  useEffect(() => {
    const timerID = setInterval(() => setDate(new Date()), 1000);

    if (date.getHours() >= 0 && date.getHours() < 12)
      setMomentDate("Bom dia, ");
    else if (date.getHours() >= 12 && date.getHours() <= 18)
      setMomentDate("Boa tarde, ");
    else setMomentDate("Boa noite, ");

    return () => clearInterval(timerID);
  }, [date]);

  useEffect(() => {
    const cookie = getCookieData();
    setName(cookie.Nome)
  }, [])

  const mock = [
    { id: 0, name: 'Janeiro' },
    { id: 1, name: 'Fevereiro' },
    { id: 2, name: 'Março' },
  ]

  const ArrowSlider = props => {
    const { className, onClick } = props
    const disabled = className.indexOf('disabled') !== -1
    const prev = className.indexOf('prev') !== -1
    return (
      <Arrow
        disabled={disabled}
        prev={prev}
        onClick={onClick}
      >
        <div style={{ width: 2, height: '50%', position: 'absolute', top: 1, left: 'calc(50% - 1px)', backgroundColor: 'white', transform: 'rotate(30deg)' }} />
        <div style={{ width: 2, height: '50%', position: 'absolute', bottom: 1, left: 'calc(50% - 1px)', backgroundColor: 'white', transform: 'rotate(-30deg)' }} />
      </Arrow>
    )
  }

  const settings = {
    dots: false,
    infinite: false,
    speed: 1000,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: <ArrowSlider />,
    nextArrow: <ArrowSlider />
  };

  const renderHeader = () => {
    if (props.location.pathname.includes('/relatorio')) {
      let extrato = props.location.pathname.includes('/extrato')
      return (
        <Fragment>
          <DivSlider bigger={!extrato}>
            {extrato ? (
              <Slider {...settings}>
                {mock.map(item => (<ItemSliderHeader title={item.name} />))}
              </Slider>
            ) : <ItemSliderHeader title={'Resgates'} />}
          </DivSlider>
          {extrato ? (
            <CardContainer>
              <Card {...register} />
              <Card {...errors} />
              <Card {...files} />
              <Card {...upload} />
            </CardContainer>
          ) : null}
        </Fragment>
      )
    }

    else {
      return (
        <Fragment>
          <DivSlider>
            <ItemSliderHeader title={'Janeiro'} />
          </DivSlider>
          <CardContainer>
            <Card {...register} />
            <Card {...errors} />
            <Card {...files} />
            <Card {...upload} />
          </CardContainer>
        </Fragment>
      )
    }
  }

  return (
    <SubHeaderContainer>
      <Container display="flex">
        <SubHeaderContent>
          <Label
            color="white"
            weight="300"
            letterSpacing={-0.45}
            size={18}
            sizeMedia={9}
            select="none"
          >
            Bem-Vindo
          </Label>
          <Label
            color="white"
            letterSpacing={-0.45}
            size={30}
            sizeMedia={15}
            marginTop={-7.5}
            select="none"
            wrap="nowrap"
            textTransform="capitalize"
          >
            {name ? name.toLowerCase() : ''}
          </Label>
        </SubHeaderContent>
        <SubHeaderContent right>
          <Container
            display="flex"
            justifyContent="flex-end"
            alignItems="center"
          >
            <Label
              color="white"
              weight="300"
              size={15}
              sizeMedia={7.5}
              select="none"
            >
              {momenteDate}
            </Label>
            <Image marginLeft={5} marginRight={5} src={IconClock} />
            <Label
              color="white"
              size={15}
              sizeMedia={7.5}
              weight="bold"
              select="none"
            >
              {`${date.getHours().toLocaleString("en-US", {
                minimumIntegerDigits: 2,
                useGrouping: false
              })}:${date.getMinutes().toLocaleString("en-US", {
                minimumIntegerDigits: 2,
                useGrouping: false
              })}`}
            </Label>
          </Container>
        </SubHeaderContent>
      </Container>

      {renderHeader()}
    </SubHeaderContainer>
  );
};

export default withRouter(SubHeader);
