import styled from "styled-components";
import { sizePxToVw } from "../../../helpers";
import { BackgroundLogin } from "../../../assets";

export const SubHeaderContainer = styled.section`
  width: calc(100% - ${sizePxToVw(78)});
  height: ${sizePxToVw(201)};
  background-image: linear-gradient(359deg, #4d4d4d, #000000),
    url(${BackgroundLogin});

  background-size: cover;
  background-repeat: no-repeat;
  background-blend-mode: multiply;

  display: flex;
  flex-direction: column;

  position: relative !important;
  z-index: 2;

  padding-left: ${sizePxToVw(39)};
  padding-right: ${sizePxToVw(39)};
  padding-top: ${sizePxToVw(39)};

  @media (max-width: 1023px) {
    height: 100px;
  }
`;

export const SubHeaderContent = styled.section`
  width: 100%;
  overflow: hidden;

  display: flex;
  flex-direction: column;
  align-items: ${({ right }) => (right ? "flex-end" : "flex-start")};
`;

export const CardContainer = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  margin-top: ${sizePxToVw(10)};

  justify-content: space-between;
  bottom: 0;
`;

export const DivSlider = styled.div`
  width: ${({ bigger }) => bigger ? '100%' : '98%'};
  height: auto;
  margin-left: ${({ bigger }) => bigger ? '0%' : '1%'};
  margin-top: ${sizePxToVw(39.3)};
`

export const Arrow = styled.span`
  position: absolute;
  top: calc(50% - 10px);
  ${({ prev }) => prev ? 'right: calc(100% + 10px)' : 'left: calc(100% + 10px)'};
  ${({ prev }) => !prev ? 'transform: rotate(180deg);' : ''}
  width: 5px;
  height: 20px;
 
  display: ${({ disabled }) => disabled ? 'none' : 'inline-block'};
 
  cursor: ${({ disabled }) => disabled ? `initial` : 'pointer'};
  z-index: 1;
`
