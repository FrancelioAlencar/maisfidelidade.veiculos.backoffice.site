import React from "react";

import { Container } from "./styles";

const Button = ({ ...props }) => {
  return <Container>{props.label}</Container>;
};

export default Button;
