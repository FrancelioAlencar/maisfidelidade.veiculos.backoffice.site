import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 280px;
  height: 60px;
  background-color: #1cb2bd;
  border-radius: 30px;
  font-family: "Open Sans";
  font-size: 15px;
  font-weight: 800;
  line-height: 1.33;
  text-align: center;
  color: #ffffff;
`;
