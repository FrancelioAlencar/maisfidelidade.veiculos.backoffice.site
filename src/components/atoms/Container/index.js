import React from 'react'
import { Item } from './styled'

const Container = ({ children, ...props }) => (children ? (
  <Item {...props}>
    {children}
  </Item>
) : null)

export default Container
