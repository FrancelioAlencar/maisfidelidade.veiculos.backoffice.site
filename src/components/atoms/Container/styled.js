import styled from 'styled-components'
import { sizePxToVw } from '../../../helpers'

export const Item = styled.div`
  
  ${({ width }) => width ? `width: ${width}` : 'width: 100%;'};
  ${({ height }) => height ? `height: ${height}` : 'height: auto'};
  ${({ minHeight }) => minHeight ? `min-height: ${minHeight};` : ''}
  ${({ minWidth }) => minWidth ? `min-width: ${minWidth};` : ''}
  ${({ marginTop }) => (marginTop ? `margin-top: ${sizePxToVw(marginTop)};` : '')}
  ${({ marginBottom }) => (marginBottom ? `margin-bottom: ${sizePxToVw(marginBottom)};` : '')}
  ${({ marginLeft }) => (marginLeft ? `margin-left: ${sizePxToVw(marginLeft)};` : '')}
  ${({ marginRight }) => (marginRight ? `margin-right: ${sizePxToVw(marginRight)};` : '')}
  ${({ cursor }) => (cursor ? `cursor: ${cursor};` : '')}
  ${({ transition }) => (transition ? `transition: ${transition};` : '')}
  ${({ display }) => (display ? `display: ${display};` : '')}
  ${({ flexDirection }) => (flexDirection ? `flex-direction: ${flexDirection};` : '')}
  ${({ justifyContent }) => (justifyContent ? `justify-content: ${justifyContent};` : '')}
  ${({ alignItems }) => (alignItems ? `align-items: ${alignItems};` : '')}
  ${({ overflowY }) => (overflowY ? `overflow-y: ${overflowY};` : '')}
  ${({ borderRadius }) => (borderRadius ? `border-radius: ${borderRadius};` : '')}
  ${({ border }) => (border ? `border: ${border};` : '')}
  ${({ padding }) => (padding ? `padding: ${padding};` : '')}
  ${({ background }) => (background ? `background: ${background};` : '')}
  ${({ boxShadow }) => (boxShadow ? `box-shadow: ${boxShadow};` : '')}
  position: relative;

`
