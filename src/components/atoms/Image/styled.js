import styled from 'styled-components'
import { sizePxToVw } from '../../../helpers'

export const Item = styled.img`
  width: ${({ width, px }) => width && !px ? `${sizePxToVw(width)}` : width ? `${width}px` : ''};
  height: ${({ height, px }) => height && !px ? `${sizePxToVw(height)}` : height ? `${height}px` : ''};
  ${({ marginTop }) => (marginTop ? `margin-top: ${sizePxToVw(marginTop)};` : '')}
  ${({ marginBottom }) => (marginBottom ? `margin-bottom: ${sizePxToVw(marginBottom)};` : '')}
  ${({ marginLeft }) => (marginLeft ? `margin-left: ${sizePxToVw(marginLeft)};` : '')}
  ${({ marginRight }) => (marginRight ? `margin-right: ${sizePxToVw(marginRight)};` : '')}
  ${({ position }) => (position ? `position:${position || 'relative'};` : '')}
  ${({ top }) => (top || top === 0 ? `top: ${sizePxToVw(top)};` : '')}
  ${({ bottom }) => (bottom || bottom === 0 ? `bottom:${sizePxToVw(bottom)};` : '')}
  ${({ left }) => (left || left === 0 ? `left:${sizePxToVw(left)};` : '')}
  ${({ right }) => (right || right === 0 ? `right: ${sizePxToVw(right)};` : '')}
  ${({ borderRadius }) => (borderRadius || borderRadius === 0 ? `border-radius: ${sizePxToVw(borderRadius)};` : '')}
  ${({ border }) => (border || border === 0 ? `border: ${border};` : '')}
  
  z-index: 1;
  @media (max-width: 1024px){
     ${({ widthMedia }) => widthMedia ? `width: ${(widthMedia)}px;` : ''};
     ${({ heightMedia }) => heightMedia ? `height: ${(heightMedia)}px;` : ''};
  } 
`