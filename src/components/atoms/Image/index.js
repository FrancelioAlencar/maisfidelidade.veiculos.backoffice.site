import React from 'react'
import { Item } from './styled'

const Image = ({ src, ...props }) => (
  <Item src={src} {...props} />
)

export default Image