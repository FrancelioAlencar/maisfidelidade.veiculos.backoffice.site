import React from "react";
import { Item } from "./styled";
import { Label } from "..";

const BubbleContainer = ({ title, ...props }) => (
    <Item>
        <Label size={20} color="#5e5e6a" weight="bold">{title || 'Útimos extratos gerados'}</Label>
    </Item>
);

export default BubbleContainer;
