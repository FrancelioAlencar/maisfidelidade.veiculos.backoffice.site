import styled from "styled-components";
import { sizePxToVw } from "../../../helpers";

export const Item = styled.section`
  width: calc(100% - ${sizePxToVw(80)});
  min-height: 80px;

  margin-bottom: 20px;

  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  background-color: #fff;
  border-radius: 10px;
  padding: 0px ${sizePxToVw(40)};
  position: relative;
  margin-bottom:  ${sizePxToVw(28)};
  &:after {
    content: "";
    width: 0;
    height: 0;
    border-top: 20px solid transparent;
    border-left: 15px solid #fff;
    border-bottom: 20px solid transparent;
    position: absolute;
    left: ${sizePxToVw(30)};
    top: calc(100% - 15px);
    transform: rotate(90deg);
    margin-bottom: 10px;
    margin-left: 60px;
  }
`;

