import styled from "styled-components";
import { sizePxToVw } from "../../../helpers";

export const Container = styled.div`
  display: flex;
  background-color: ${({ color }) => color || "#000"};
  width: calc(25% - 25px);
  height: ${({ height }) => (height ? `${sizePxToVw(height)}` : "auto")};
  border-radius: 20px;
  box-shadow: 20px 7px 40px 0 rgba(0, 0, 0, 0.08);
  align-content: center;
  align-items: center;
`;

export const Content = styled.div`
  display: flex;
  height: 50px;
  width: 100%;
  align-content: center;
  justify-content: center;
  align-items: center;
  background-color: ${({ color }) => color || "#ffffff"};
`;
export const Info = styled.div`
  display: flex;
  flex: 1;
  height: ${sizePxToVw(55)};
  margin-left: ${sizePxToVw(40)};
  align-content: center;
  align-items: center;
  font-family: "Open Sans";
  font-size: ${sizePxToVw(40)};
  font-weight: bold;
  line-height: 1.38;
  text-align: left;
  color: ${({ colorInfo }) => colorInfo || "#1cb2bd"};
`;

export const TextInfo = styled.div`
  display: flex;
  flex: 2;
  width: ${sizePxToVw(114)};
  height: ${sizePxToVw(49)};
  margin-right: ${sizePxToVw(40)};
  margin-left: ${sizePxToVw(20)};
  align-content: center;
  align-items: center;
  font-family: "Open Sans";
  font-size: ${sizePxToVw(14)};
  line-height: 1.07;
  text-align: left;
  color: ${({ colorText }) => colorText || "#ffffff"};
`;
