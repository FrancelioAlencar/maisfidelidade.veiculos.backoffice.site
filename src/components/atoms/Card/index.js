import React from "react";
import Image from "../Image";
import { IconPlus } from "../../../assets/index";

import { Container, Content, Info, TextInfo } from "./styles";

const Card = ({ ...props }) => {
  return (
    <Container {...props}>
      <Content {...props}>
        <Info {...props}>
          {props.image ? (
            <Image src={IconPlus} width={60} height={60} />
          ) : (
            props.textInfo
          )}
        </Info>
        <TextInfo {...props}>{props.text}</TextInfo>
      </Content>
    </Container>
  );
};

export default Card;
