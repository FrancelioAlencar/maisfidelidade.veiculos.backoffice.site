import React from "react";
import { Label } from "..";

const ItemSliderHeader = ({ title }) => (
    <div>
        <div style={{ display: 'flex', flexDirection: 'row', flexWrap: 'nowrap', alignItems: 'center' }}>
            <div style={{ display: 'flex', flex: 1, opacity: 0.17, height: 2, backgroundColor: 'white' }} />
            <Label size={28} color="#FFFFFF" marginLeft={40} marginRight={40}>{title}</Label>
            <div style={{ display: 'flex', flex: 1, opacity: 0.17, height: 2, backgroundColor: 'white' }} />
        </div>
    </div>

);

export default ItemSliderHeader;
