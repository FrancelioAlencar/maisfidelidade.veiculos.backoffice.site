import React from 'react'
import { Item } from './styled'

const Label = ({ ...props }) => (
  <Item
    {...props}
  >
    {props.children}
  </Item>
)

export default Label
