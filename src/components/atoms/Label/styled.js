import styled from 'styled-components'
import { sizePxToVw } from '../../../helpers'

export const Item = styled.span`
  display: ${({ display }) => (display ? `${display}` : 'block')};
  vertical-align: ${({ display }) => (display ? 'middle' : 'initial')};
  color: ${({ color }) => color || '#000'};
  width: ${({ width }) => (width ? `${width}px` : 'auto')};
  cursor: ${({ cursor }) => cursor ? cursor : 'default'};

  font-size: ${({ size }) => (size ? `${sizePxToVw(size)}` : `${sizePxToVw(15)}`)};
  line-height: ${({ lineHeight }) => (lineHeight ? `${sizePxToVw(lineHeight)}` : 'initial')};
  letter-spacing: ${({ letterSpacing }) => (letterSpacing ? `${sizePxToVw(letterSpacing)}` : 'initial')};
  font-weight: ${({ weight }) => (weight || 'normal')};
  margin-bottom: ${({ marginBottom }) => (marginBottom ? `${sizePxToVw(marginBottom)}` : '0')};
  margin-top: ${({ marginTop }) => (marginTop ? `${sizePxToVw(marginTop)}` : '0')};
  margin-left: ${({ marginLeft }) => (marginLeft ? `${sizePxToVw(marginLeft)}` : '0')};
  margin-right: ${({ marginRight }) => (marginRight ? `${sizePxToVw(marginRight)}` : '0')};
  white-space: ${({ wrap }) => (wrap ? `${wrap}` : 'initial')};
  font-family: ${({ fontFamily }) => fontFamily || 'Open Sans'};
  text-decoration-line: ${({ textDecoration }) => textDecoration || 'none'};
  ${({ select }) => select ? `user-select: ${select};` : ''}
  ${({ textTransform }) => textTransform ? `text-transform: ${textTransform};` : ''}
  position: relative;
  z-index: 1;

  @media (max-width: 1024px){
    ${({ sizeMedia }) => sizeMedia ? `font-size: ${(sizeMedia)}px;` : ''};
 } 

  &:after {
    content: ${({ suffix }) => (suffix ? `"${suffix}"` : '')};
    font-size: ${sizePxToVw(24)};
    margin-top: ${sizePxToVw(12)};
    vertical-align: top;
    display: inline-block;
    margin-left: 0px;
  }
`
