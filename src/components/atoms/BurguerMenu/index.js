import React from 'react'
import './styles.scss'

const BurguerMenu = ({ checked, onClick }) => {
  return (
    <div
      className="wrapper"
    >
      <input
        type="checkbox"
        checked={checked}
      />
      <div className="bun">
        <div className="burger" />
      </div>
    </div>
  )
}

export default BurguerMenu
