import styled from "styled-components";
import { sizePxToVw } from "../../../helpers";

export const Container = styled.div`
  display: flex;
  width: 100%;
  height: ${sizePxToVw(300)};
  border-radius: ${sizePxToVw(10)};
  box-shadow: 0 2px 16px 0 rgba(0, 0, 0, 0.1);
  background-color: #ffffff;
  align-items: center;
  justify-content: center;
  align-content: center;
  margin-bottom: 80px;
`;

export const DropContainer = styled.div`
  width: 80%;
  height: ${sizePxToVw(220)};
  display: flex;
  align-items: center;
  justify-content: center;
  border: 1px dashed #979797;
  border-radius: 5px;
  cursor: pointer;
  text-align: center;
  .p {
    font-size: 14px;
    margin: 0;
  }
`;
export const PermitedFile = styled.div`
  border: 1px dashed #335c85;
`;
export const NotPermitedFile = styled.div`
  border: 1px dashed red;
`;
