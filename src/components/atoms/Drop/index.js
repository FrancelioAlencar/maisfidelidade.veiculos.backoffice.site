import React from "react";
import Dropzone from "react-dropzone";
import {
  DropContainer,
  PermitedFile,
  NotPermitedFile,
  Container
} from "./styles";

const Drop = props => {
  return (
    <Container>
      <Dropzone accept={`${props.accept}/*`} onDropAccepted={props.onUpload}>
        {({ getRootProps, getInputProps, isDragActive, isDragReject }) => (
          <DropContainer {...getRootProps()}>
            {isDragActive ? <PermitedFile /> : <NotPermitedFile />}
            <input {...getInputProps()} />
            <p>{props.title || "Insira um arquivo"}</p>
          </DropContainer>
        )}
      </Dropzone>
    </Container>
  );
};

export default Drop;
