import React, { useState } from 'react'
import { DoLogin } from '../../../actions'
import { Container, Image, Label } from '../../atoms'
import { sizePxToVw } from '../../../helpers'
import { LogoWhite, SantanderRed, IconLogin, IconPassword, SadFace } from '../../../assets'
import { Background, DivInput, Input, CheckBox, Button, Modal, ContentModal, DivSad, Divisor, Close, PlusSign1, PlusSign2, PlusSign3, PlusSign4 } from './styled'
import { withRouter } from 'react-router-dom'



const Login = () => {
  const [login, setLogin] = useState(null)
  const [password, setPassword] = useState(null)
  const [check, setCheck] = useState(true)
  const [open, setOpen] = useState(false)

  const Logar = event => {
    event.preventDefault()
    event.stopPropagation()

    if (login && password)
      DoLogin({ login, password, check }).then(resp => {
        window.location.href = ""
      })
    else
      alert('Preencha login e senha')
  }

  const toggleModal = event => {
    event.preventDefault()
    event.stopPropagation()

    setOpen(!open)
  }

  return (
    <Container
      display="flex"
      flexDirection="column"
      alignItems="center"
      justifyContent="center"
      height="100%"
      background="#F2F3F6"
    >

      <PlusSign1>+</PlusSign1>
      <PlusSign2>+</PlusSign2>
      <PlusSign3>+</PlusSign3>
      <PlusSign4>+</PlusSign4>

      <Modal active={open}>
        <ContentModal>
          <Close onClick={toggleModal} />
          <DivSad>
            <Image src={SadFace} width={49.1} />
          </DivSad>
          <Label weight="bold" size={29} lineHeight={32} letterSpacing={-.24} color="#4d4c59" marginTop={10}>Esqueceu seu login?</Label>
          <Label size={16} lineHeight={22} color="#4d4c59" marginTop={20} marginBottom={20}>
            Seu e-mail e senha não existem<br /> ou estão incorretos.
          </Label>
          <Divisor />
          <Label size={12} color="#4d4c59" marginTop={20}>
            Entre em contato com o administrador do sistema
          </Label>
        </ContentModal>
      </Modal>
      <Image src={LogoWhite} width={310} />
      <Label marginTop={20} marginBottom={41} color="#ffffff">Bem Vindo, entre com seus dados de login.</Label>
      <Background />
      <Container
        display="flex"
        flexDirection="column"
        alignItems="center"
        width={sizePxToVw(400)}
        height={sizePxToVw(280)}
        minWidth={sizePxToVw(400)}
        minHeight={sizePxToVw(280)}
        borderRadius="20px"
        padding={`${sizePxToVw(60)} ${sizePxToVw(40)}`}
        background="white"
        boxShadow="0 3px 17px 0 rgba(44, 40, 40, 0.11)"
      >
        <DivInput marginBottom="20px">
          <Image src={IconLogin} />
          <Input
            placeholder="Email"
            type="email"
            value={login}
            onChange={event => setLogin(event.target.value)}
          />
        </DivInput>
        <DivInput>
          <Image src={IconPassword} />
          <Input
            placeholder="********"
            type="password"
            value={password}
            onChange={event => setPassword(event.target.value)}
          />
        </DivInput>
        <Container marginTop={20} marginBottom={43} display="flex" justifyContent="space-between" alignItems="center">
          <Container display="flex" alignItems="center">
            <CheckBox check={check} onClick={() => setCheck(!check)} />
            <Label select="none" onClick={() => setCheck(!check)} cursor="pointer" wrap="nowrap" size={13} color="#6b6a6a">Lembrar-me</Label>
          </Container>
          <Label onClick={toggleModal} select="none" cursor="pointer" wrap="nowrap" size={13} color="#6b6a6a">Esqueci a senha?</Label>
        </Container>
        <Button onClick={Logar}>
          <Label select="none" cursor="pointer" wrap="nowrap" size={15} color="#FFF">FAZER LOGIN</Label>
        </Button>
      </Container>
      <Image src={SantanderRed} marginTop={76} width={140} />
    </Container>
  )
}

export default withRouter(Login)
