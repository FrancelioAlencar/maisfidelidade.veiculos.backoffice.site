import styled from "styled-components";
import { BackgroundLogin } from '../../../assets'
import { sizePxToVw } from "../../../helpers";

export const Background = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  height: 50%;
  width: 100%;
  background-size: cover;
  background-color: #a2040d;
  background-image: url(${BackgroundLogin});
  background-repeat: no-repeat;
  border-bottom: 5px solid #37b5be;
  z-index: 0;
  background-blend-mode: multiply;
`

export const DivInput = styled.div`
  width: calc(100% - ${sizePxToVw(44)});
  height: ${sizePxToVw(60)};
  border: solid 1px #e8e8e8;
  background-color: #f7f5f5;
  border-radius: 5px;
  margin-bottom: ${({ marginBottom }) => (marginBottom || '0')};
  display: flex;
  align-items: center;
  padding: 0 ${sizePxToVw(22)};
`

export const Input = styled.input`
  width: 100%;
  height: ${sizePxToVw(25)};
  background: transparent;
  background-color: transparent;
  border: none;
  outline: none;
  margin-left: ${sizePxToVw(20.1)};
  font-size: ${sizePxToVw(15)};
  color: #a19f9f;

  ::placeholder {
    color: #a19f9f;
    background: transparent;
    background-color: transparent;
  }
  &:-webkit-autofill  {
    -webkit-box-shadow: 0 0 0px 1000px #f7f5f5 inset;
    font-size: ${sizePxToVw(15)};
    color: #a19f9f !important;
  }
`

export const Button = styled.div`
  width: ${sizePxToVw(200)};
  height: ${sizePxToVw(60)};
  border-radius: 30px;
  background-color: #37b5be;
  
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;

`

export const CheckBox = styled.div`
  width: ${sizePxToVw(20)};
  height: ${sizePxToVw(20)};
  border-radius: 6px;
  border: solid 1px #ececec;
  background-color: #f8f6f6;
  position: relative;
  cursor: pointer;
  margin-right: ${sizePxToVw(9)};
  
  &:after{
    content: ${({ check }) => check ? '""' : '""'};
    position: absolute;
    top: ${({ check }) => check ? `${sizePxToVw(0)}` : `${sizePxToVw(10)}`};
    left: ${sizePxToVw(3.5)};
    width: ${({ check }) => check ? `${sizePxToVw(20.7)}` : `${sizePxToVw(0)}`};
    height: ${({ check }) => check ? `${sizePxToVw(8.2)}` : `${sizePxToVw(0)}`};
    transform: rotate(-45deg);
    border-bottom: ${({ check }) => check ? `${sizePxToVw(4)}` : `0`} solid #37b5be;    
    border-left: ${({ check }) => check ? `${sizePxToVw(4)}` : `0`} solid #37b5be;   
    transition: all .3s ease-in-out; 
  }
`

export const Modal = styled.div`
  width: 100vw;
  height: 100vh;
  position: absolute;
  top: 0;
  left: 0;
  z-index: 9999;

  display: ${({ active }) => active ? 'flex' : 'none'};
  align-items: center;
  justify-content: center;

  background: rgba(0,0,0,.88);
`

export const ContentModal = styled.div`
  position: relative;

  width: ${sizePxToVw(480 - (60 * 2))};
  height: ${sizePxToVw(360 - (62 * 2))};
  background-color: #ffffff;
  border-radius: 20px;
  opacity: 1;

  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;

  padding: ${sizePxToVw(62)} ${sizePxToVw(60)};
`

export const DivSad = styled.div`
  width: ${sizePxToVw(64)};
  height: ${sizePxToVw(64)};
  border-radius: 50%;
  background-color: #F9002D;

  display: flex;
  align-items: center;
  justify-content: center;

`

export const Divisor = styled.span`
  width: 100%;
  height: 0;
  border: solid 1px #ecedf2;
`
export const Close = styled.div`
  position: absolute;
  right: ${sizePxToVw(41)};
  top: ${sizePxToVw(46)};
  width: ${sizePxToVw(20)};
  height: ${sizePxToVw(20)};
  cursor: pointer;
  opacity: 0.3;

  &:hover {
    opacity: 1;
  }
  
  &:before, &:after {
    position: absolute;
    left: ${sizePxToVw(15)};
    content: '';
    height: ${sizePxToVw(20)};
    width: ${sizePxToVw(2.5)};
    background-color: #4d4c59;
    border-radius: 5px;
  }

  &:before {
    transform: rotate(45deg);
  }

  &:after {
    transform: rotate(-45deg);
  }
`

export const PlusSign1 = styled.p`
  width: ${sizePxToVw(228)};
  height: ${sizePxToVw(147)};
  opacity: 0.53;
  -webkit-text-stroke: ${sizePxToVw(1)} #ffffff;
  font-family: 'Open Sans';
  font-size: ${sizePxToVw(400)};;
  font-weight: bold;
  line-height: 0;
  color: transparent;

  position: absolute;
  top: ${sizePxToVw(-20)};
  left: ${sizePxToVw(382)};
  z-index: 2;
`

export const PlusSign2 = styled.p`
  width: ${sizePxToVw(46)};
  opacity: 0.5;
  -webkit-text-stroke: ${sizePxToVw(1)} red;
  font-family: 'Open Sans';
  font-size: ${sizePxToVw(80)};;
  font-weight: bold;
  text-align: left;
  color: transparent;

  position: absolute;
  top: ${sizePxToVw(80)};
  left: ${sizePxToVw(306)};
  z-index: 2;
`

export const PlusSign3 = styled.p`
  width: ${sizePxToVw(29)};
  height: ${sizePxToVw(68)};
  -webkit-text-stroke: ${sizePxToVw(1)} #ff3232;
  font-family: 'Open Sans';
  font-size: ${sizePxToVw(50)};;
  font-weight: bold;
  text-align: left;
  color: transparent;

  position: absolute;
  top: ${sizePxToVw(15)};
  left: ${sizePxToVw(934)};
  z-index: 2;
`

export const PlusSign4 = styled.p`
  width: ${sizePxToVw(80)};
  height: ${sizePxToVw(191)};
  -webkit-text-stroke: ${sizePxToVw(1)} #ff3232;
  font-family: 'Open Sans';
  font-size: ${sizePxToVw(140)};;
  font-weight: bold;
  text-align: left;
  color: transparent;

  position: absolute;
  top: ${sizePxToVw(92)};
  left: ${sizePxToVw(1042)};
  z-index: 2;
`