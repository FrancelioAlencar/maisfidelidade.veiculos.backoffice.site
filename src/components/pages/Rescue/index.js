import React from "react";
import { ReportRescue, HistoricRescue } from "../../molecules";
import { Container } from "../../atoms";

const Rescue = () => (
    <Container>
        <ReportRescue />
        <HistoricRescue />
    </Container>
);

export default Rescue;