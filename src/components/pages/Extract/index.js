import React from "react";
import { DashTable } from "../../molecules";
const mockWeek = [
    { id: 0, name: 'Semana45.xls', date: '01/FEV' },
    { id: 1, name: 'Semana46.xls', date: '05/FEV' },
    { id: 2, name: 'Semana47.xls', date: '12/FEV' },
    { id: 3, name: 'Semana48.xls', date: '17/FEV' },
]

let mockMonth = [
    { id: 0, name: 'Mes04.xls', date: '01/ABR', rescue: false },
    { id: 1, name: 'Mes03.xls', date: '01/ABR', rescue: true },
]

const Extract = () => (
    <div style={{ position: "relative", zindex: 0 }}>
        <DashTable size={20} color="#5e5e6a" weight="bold" title="Extratos Semanais" data={mockWeek} />
        <DashTable size={20} color="#5e5e6a" weight="bold" title="Extrato Mensal" data={mockMonth} />
    </div>
);

export default Extract;