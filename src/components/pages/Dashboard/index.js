import React from "react";
import { DashTable } from "../../molecules";

const Dashboard = () => (
  <div style={{ position: "relative", zindex: 0 }}>
    <DashTable size={20} color="#5e5e6a" weight="bold" />
  </div>
);

export default Dashboard;
