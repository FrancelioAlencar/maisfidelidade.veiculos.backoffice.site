import styled from "styled-components";
import { sizePxToVw } from "../../../helpers";
import { BackgroundLogin } from "../../../assets";

export const Container = styled.section`
  width: 100vw;
  height: 100vh;
  overflow: hidden;

  display: flex;
  flex-direction: row;

  position: relative;
  z-index: 1;
`;

export const Menu = styled.section`
  width: 60px;
  min-width: 60px;
  max-width: 60px;
  height: 100vh;
  overflow: hidden;
  background-color: #a00000;

  display: flex;
  flex-direction: column;
  align-items: center;
  transition: all 0.5s ease-in-out;
`;

export const SubLinks = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  margin-top: ${sizePxToVw(54)};
`;

export const ItemMenu = styled.div`
  width: 60px;
  height: 60px;
  background-color: ${({ active }) => (active ? "#920b13" : "")};

  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  position: relative;
  margin-bottom: ${({ first }) => (first ? `${sizePxToVw(15)}` : "0")};

  &:before {
    content: ${({ active }) => (active ? '""' : "")};
    width: 3px;
    height: 100%;
    background-color: #0093ee;

    position: absolute;
    left: 0;
    top: 0;
  }
`;

export const SubMenu = styled.section`
  width: ${({ open }) => (open ? `240px` : "0")};
  height: 100vh;
  overflow: hidden;
  background-color: #920b13;
  transition: all 0.5s ease-in-out;

  position: relative;

  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: ${({ open }) =>
    open ? `${sizePxToVw(17)} ${sizePxToVw(20)}` : "0"};
  margin: 0;

  @media (max-width: 1023px) {
    position: absolute;
    left: 60px;
    top: 0;
    width: ${({ open }) => (open ? `240px` : "0")};
    z-index: 2;
  }
`;

export const Content = styled.section`
  width: 100%;
  height: 100%;
  overflow: hidden;
  background-color: #f2f3f6;
  position: relative;
  z-index: 1;
`;

export const Children = styled.section`
  height: calc(100% - ${sizePxToVw(300)} - ${({ pTop }) => pTop ? `${sizePxToVw(pTop)}` : '0'});
  width: calc(100% - ${sizePxToVw(78)});
  padding-top: ${({ pTop }) => pTop ? `${sizePxToVw(pTop)}` : '0'};
  padding-left: ${sizePxToVw(39)};
  padding-right: ${sizePxToVw(39)};
  overflow-y: auto;
  overflow-x: hidden;
  position: relative;
  z-index: 1;
  &::-webkit-scrollbar {
    display: none;
  }
  
  /* Hide scrollbar for IE and Edge */
  -ms-overflow-style: none;
`;

export const SubLink = styled.div`
  width: calc(100% - ${sizePxToVw(23)};);
  height: ${sizePxToVw(20)};
  background-color: ${({ active }) => active ? '#00b2bc' : ''};
  margin-top: ${sizePxToVw(20)};
  border-radius: 2px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding-left: ${sizePxToVw(23)};
  padding-right: ${sizePxToVw(10)};
  text-decoration: none;
`

export const TriangleSublink = styled.span`
  width: 0; 
  height: 0; 
  border-left: 5px solid transparent;
  border-right: 5px solid transparent;
  
  border-bottom: 5px solid white;

  transform: rotate(90deg);
`
