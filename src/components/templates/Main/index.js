import React, { useState, useEffect, Fragment } from "react";
import {
  Container as ContainerMain,
  Menu,
  ItemMenu,
  Content,
  SubMenu,
  Children,
  SubLinks,
  SubLink,
  TriangleSublink
} from "./styled";
import { Label, Image, BurguerMenu } from "../../atoms";
import { LogoWhite, Santander } from "../../../assets/";
import { routes } from "../../../helpers";
import { withRouter, Link } from "react-router-dom";
import { Header, SubHeader } from "../../organisms";

const Main = ({ children, ...props }) => {
  const [openSubMenu, setOpenSubMenu] = useState(window.innerWidth > 1023);

  const toggleSubMenu = event => {
    event.preventDefault();
    event.stopPropagation();
    setOpenSubMenu(!openSubMenu);
  };

  let paddingPage = () => {
    if (props.location.pathname.includes('/relatorio/resgate')) {
      return 40
    }
    return 80
  }

  return (
    <ContainerMain>
      <Menu>
        <ItemMenu first onClick={toggleSubMenu}>
          <BurguerMenu checked={!openSubMenu} />
        </ItemMenu>
        {routes
          .filter(item => item.src)
          .map(item => (
            <Link key={item.id} to={item.path}>
              <ItemMenu
                active={
                  props &&
                  props.location &&
                  props.location.pathname.indexOf(item.path) !== -1
                }
              >
                <Image src={item.src} width={24} height={24} px />
              </ItemMenu>
            </Link>
          ))}
      </Menu>
      <SubMenu open={openSubMenu}>
        <Image src={LogoWhite} />
        <SubLinks>
          {
            routes
              .find(item => props.location.pathname.includes(item.path)) &&
            routes
              .find(item => props.location.pathname.includes(item.path)).subLinks &&
            routes
              .find(item => props.location.pathname.includes(item.path))
              .subLinks.map(item => (
                <Fragment>
                  <Label
                    color="white"
                    weight="300"
                    letterSpacing={-0.45}
                    size={18}
                    sizeMedia={18}
                  >
                    {item.title}
                  </Label>
                  {item.lst && item.lst.map(sublink => (
                    <Link style={{ textDecoration: 'none' }} key={sublink.id} to={sublink.path}>
                      <SubLink active={props.location.pathname.includes(sublink.path)}>
                        <Label cursor="pointer" size={14} color="#f3f8fe">{sublink.name}</Label>
                        {
                          props.location.pathname.includes(sublink.path) ? <TriangleSublink /> : null
                        }
                      </SubLink>
                    </Link>
                  ))}
                </Fragment>
              ))}
        </SubLinks>
        <Image src={Santander} marginBottom={50} />
      </SubMenu>
      <Content>
        <Header />
        <SubHeader />
        <Children pTop={paddingPage()}>{children}</Children>
      </Content>
    </ContainerMain>
  );
};

export default withRouter(Main);
