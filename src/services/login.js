import Axios from "axios";

const Login = ({ login, password }) => {
  return new Promise((resolve, reject) => {
    try {
      Axios.post(
        "https://dev-api-maisfidelidade-backoffice.santanderfinanc.com.br/v1/api/Autenticacao/autenticar",
        {
          Email: login,
          Senha: password,
          ManterConectado: false
        }
      )
        .then(res => {
          const token = res.data.objeto.authToken;
          resolve(decodeURIComponent(token));
        })
        .catch(error => {
          console.log({ error });
        });
    } catch (error) {
      console.log("error => ", error);
    }
  });
};

export default Login;
